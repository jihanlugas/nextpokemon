module.exports = {
	purge: [],
	darkMode: false, // or 'media' or 'class'
	theme: {
		extend: {
			keyframes: {
				fadeIn: {
					'0%': {
						opacity: 0,
						transform: 'translateX(-1rem)',
					},
					'100%': {
						opacity: 1,
						transform: 'translateX(0)',
					},
				},
			},
			animation: {
				fadeIn: 'fadeIn 1s'
			},
		}
	},
	variants: {
		extend: {
			animation: ['hover', 'focus'],
		},
	},
	plugins: [],
};
