import React, { useContext, useEffect, useState } from 'react';
import Head from 'next/head';
import Main from '@com/Layout/Main';
import { BiChevronRight } from 'react-icons/bi';
import Link from 'next/link';
import { GetServerSideProps } from 'next';
import { useMutation, useQuery } from 'react-query';
import { Api } from '@lib/Api';
import { isEmptyObject } from '@utils/Validate';
import { AiOutlineLoading3Quarters } from 'react-icons/ai';
import Image from "next/image";
import { Fetch } from '@lib/Fetch';
import AppContext from '@stores/appContext';
import ModalMyPokemon from '@com/Modal/ModalMyPokemon';

interface Props {
  pokemon: string
}

interface PropsSpecies {
  name: string
  url: string
}

interface Ipokemon {
  base_experience?: number,
  height?: number,
  id?: number,
  is_default?: boolean,
  name?: string,
  weight?: number,
  species?: {
    name?: string,
    url?: string,
  }
}



// const Species: React.FC<PropsSpecies> = (props) => {
//   const { name, url } = props
//   const { isLoading, error, data, isFetching, refetch } = useQuery('pokemonspecies', () => Fetch.get(url));

//   console.log("data ", data)

//   return (
//     <div>Species</div>
//   )
// }

const Pokemon: React.FC<Props> = (props) => {
  const { notif } = useContext(AppContext);
  const { pokemon } = props
  const [datapokemon, setDatapokemon] = useState<Ipokemon>({})
  const [show, setShow] = useState<boolean>(false)
  const [selectedId, setSelectedId] = useState<number>(0)

  const { isLoading, error, data, isFetching, refetch } = useQuery('pokemon', () => Api.get('/pokemon/' + pokemon));

  const { mutate: mutateCatch, isLoading: isLoadingMutation } = useMutation((val: { pokemon: string }) => Api.post('/pokemon/catch', val));
  const { mutate: mutatePokemon, isLoading: isLoadingPokemon } = useMutation((val: { userpokemonId: number, nickname: string }) => Api.post('/user-pokemon/my-pokemon', val));

  const handleCatch = (pokemon: string) => {
    mutateCatch({ pokemon }, {
      onSuccess: (res) => {
        console.log("handleCatch ", res)
        if (res.success) {
          notif.error(res.message)
          setShow(true)
          setSelectedId(res.payload.userpokemonId)
        } else {
          notif.error(res.message)
        }

      }
    })
  }

  const onClickOverlay = (userpokemonId: number = 0, refresh: boolean = false) => {
    setSelectedId(userpokemonId)
    setShow(!show)
    if (refresh) {
      refetch()
    }
  }

  useEffect(() => {
    if (data && data.success) {
      if (data.payload && data.payload)
        setDatapokemon(data.payload);
    }
  }, [data]);


  return (
    <Main>
      <Head>
        <title>{'Pokemon ' + pokemon}</title>
      </Head>
      <div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
        <div className={'mb-4 flex text-2xl items-center'}>
          <Link href={{ pathname: '/pokemon' }}>
            <a>
              <div className={'mr-2'} >Pokemon</div>
            </a>
          </Link>
          <BiChevronRight className={'mr-2'} size={'1em'} />
          <div className={'mr-2'} >{pokemon}</div>
        </div>
        <div className={'mb-4'}>
          {isLoading ? (
            <div className={'w-full h-60 flex justify-center items-center'}>
              <AiOutlineLoading3Quarters className={'animate-spin font-bold'} size={'4em'} />
            </div>
          ) : (
            <div>
              {isEmptyObject(datapokemon) ? (
                <div>Data Not Found</div>
              ) : (
                <div className={''}>
                  <div className={"flex flex-col sm:flex-row"}>
                    <div className={'w-full bg-gray-50 rounded-md shadow-lg mb-4 p-2'}>
                      <div className={'p-2 w-full'}>
                        <Image src={'/images/pokemon-3.png'} alt={'pokemon.name'} layout={'responsive'} width={150} height={150} />
                      </div>
                      <div className={'p-2 w-full flex justify-center'}>
                        <div className={"bg-green-400 py-2 px-8 rounded-full text-white cursor-pointer font-semibold"} onClick={() => handleCatch(datapokemon.name)}>
                          Catch
                        </div>
                      </div>
                    </div>
                    <div className={"w-full p-0 sm:p-4 mb-8"}>
                      <div className={"text-lg font-semibold capitalize mb-4"}>
                        {datapokemon.name}
                      </div>
                      <div className={"grid gap-4 grid-cols-2 w-full"}>
                        <div className={'w-full'}>
                          Height
                        </div>
                        <div className={'w-full font-semibold'}>
                          {datapokemon.height}
                        </div>
                      </div>
                      <div className={"grid gap-4 grid-cols-2 w-full"}>
                        <div className={'w-full'}>
                          Weight
                        </div>
                        <div className={'w-full font-semibold'}>
                          {datapokemon.weight}
                        </div>
                      </div>
                      <div className={"grid gap-4 grid-cols-2 w-full"}>
                        <div className={'w-full'}>
                          Best Experience
                        </div>
                        <div className={'w-full font-semibold'}>
                          {datapokemon.base_experience}
                        </div>
                      </div>
                    </div>
                  </div>
                  <ModalMyPokemon
                    show={show}
                    onClickOverlay={onClickOverlay}
                    selectedId={selectedId}
                  />
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    </Main >
  );
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { pokemon } = context.query;

  return {
    props: {
      pokemon: pokemon
    }
  };
};


export default Pokemon;