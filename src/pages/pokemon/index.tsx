import Main from "@com/Layout/Main";
import { Api } from "@lib/Api";
import Head from "next/head";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import { AiOutlineLoading3Quarters } from "react-icons/ai";
import { useQuery } from "react-query";
import Link from 'next/link';



interface Ipokemon {
  name?: string,
  url?: string,
  image?: string,
}


const Pokemon = () => {

  const { isLoading, error, data, isFetching, refetch } = useQuery('pokemon', () => Api.get('/pokemon', { limit: 20, offset: 0 }));
  const [pokemons, setPokemons] = useState<Ipokemon[]>([])

  useEffect(() => {
    if (data && data.success) {
      if (data.payload && data.payload.results)
        setPokemons(data.payload.results);
    }
  }, [data]);

  return (
    <Main>
      <Head>
        <title>Pokemon</title>
      </Head>
      <div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
        <div className={'mb-4'}>
          <div className={'mb-4 flex justify-between items-center'}>
            <div className={'text-2xl'} >
              Pokemon
            </div>
          </div>
        </div>
        <div className={'mb-4'}>
          {isLoading ? (
            <div className={'w-full h-60 flex justify-center items-center'}>
              <AiOutlineLoading3Quarters className={'animate-spin font-bold'} size={'4em'} />
            </div>
          ) : (
            <div>
              {pokemons.length > 0 ? (
                <div className="grid gap-4 grid-cols-2 sm:grid-cols-4 md:grid-cols-5">
                  {pokemons.map((pokemon, key) => {
                    const image = pokemon.image === "" ? "/images/pokemon-3.png" : pokemon.image
                    return (
                      <Link href={`/pokemon/${pokemon.name}`} key={key}>
                        <a>
                          <div className={'bg-gray-50 rounded-md shadow-lg'}>
                            <div className={'p-2 w-full'}>
                              <Image src={image} alt={'pokemon.name'} layout={'responsive'} width={150} height={150} />
                            </div>
                            <div className={'text-center p-2 capitalize font-semibold'}>
                              {pokemon.name}
                            </div>
                          </div>
                        </a>
                      </Link>
                    );
                  })}
                </div>
              ) : (
                <div>
                  <div>Data Not Found</div>
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    </Main>
  )
}

export default Pokemon;
