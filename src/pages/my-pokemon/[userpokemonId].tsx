import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import Main from '@com/Layout/Main';
import { BiChevronRight } from 'react-icons/bi';
import Link from 'next/link';
import { GetServerSideProps } from 'next';
import { useQuery } from 'react-query';
import { Api } from '@lib/Api';
import { isEmptyObject } from '@utils/Validate';
import { AiOutlineLoading3Quarters } from 'react-icons/ai';
import Image from "next/image";
import { Fetch } from '@lib/Fetch';

interface Props {
  userpokemonId: number
}

interface PropsSpecies {
  name: string
  url: string
}

interface Ipokemon {
  base_experience?: number,
  height?: number,
  id?: number,
  is_default?: boolean,
  name?: string,
  weight?: number,
  species?: {
    name?: string,
    url?: string,
  }
}

interface Imypokemon {
  userpokemonId?: number,
  userId?: number,
  nickname?: string,
  pokemon?: string,
}

interface Imypokemondetail {
  myPokemon?: Imypokemon,
  pokemon?: Ipokemon,
}


const Pokemon: React.FC<Props> = (props) => {
  const { userpokemonId } = props
  const [datapokemon, setDatapokemon] = useState<Imypokemondetail>({})

  const { isLoading, error, data, isFetching, refetch } = useQuery('my-pokemon', () => Api.get('/user-pokemon/my-pokemon/' + userpokemonId));

  const handleCatch = () => {

  }

  useEffect(() => {
    if (data && data.success) {
      if (data.payload && data.payload)
        setDatapokemon(data.payload);
    }
  }, [data]);


  console.log("datapokemon ", datapokemon)



  return (
    <Main>
      <Head>
        <title>{'Pokemon ' + (datapokemon && datapokemon.myPokemon && datapokemon.myPokemon.nickname)}</title>
      </Head>
      <div className={'p-4 font-sans w-full max-w-4xl mx-auto'}>
        <div className={'mb-4 flex text-2xl items-center'}>
          <Link href={{ pathname: '/my-pokemon' }}>
            <a>
              <div className={'mr-2'} >My Pokemon</div>
            </a>
          </Link>
          <BiChevronRight className={'mr-2'} size={'1em'} />
          <div className={'mr-2'} >{datapokemon && datapokemon.myPokemon && datapokemon.myPokemon.nickname}</div>
        </div>
        <div className={'mb-4'}>
          {isLoading ? (
            <div className={'w-full h-60 flex justify-center items-center'}>
              <AiOutlineLoading3Quarters className={'animate-spin font-bold'} size={'4em'} />
            </div>
          ) : (
            <div>
              {isEmptyObject(datapokemon) ? (
                <div>Data Not Found</div>
              ) : (
                <div className={''}>
                  <div className={"flex flex-col sm:flex-row"}>
                    <div className={'w-full bg-gray-50 rounded-md shadow-lg mb-4 p-2'}>
                      <div className={'p-2 w-full'}>
                        <Image src={'/images/pokemon-3.png'} alt={'pokemon.name'} layout={'responsive'} width={150} height={150} />
                      </div>
                      <div className={'text-center p-2 capitalize'}>
                        <div className={"font-semibold text-lg"}>
                          {datapokemon.myPokemon.nickname}
                        </div>
                        <div className={""}>
                          {datapokemon.myPokemon.pokemon}
                        </div>
                      </div>
                    </div>
                    <div className={"w-full p-0 sm:p-4 mb-8"}>
                      <div className={"text-lg font-semibold capitalize mb-4"}>
                        {datapokemon.pokemon.name}
                      </div>
                      <div className={"grid gap-4 grid-cols-2 w-full"}>
                        <div className={'w-full'}>
                          Height
                        </div>
                        <div className={'w-full font-semibold'}>
                          {datapokemon.pokemon.height}
                        </div>
                      </div>
                      <div className={"grid gap-4 grid-cols-2 w-full"}>
                        <div className={'w-full'}>
                          Weight
                        </div>
                        <div className={'w-full font-semibold'}>
                          {datapokemon.pokemon.weight}
                        </div>
                      </div>
                      <div className={"grid gap-4 grid-cols-2 w-full"}>
                        <div className={'w-full'}>
                          Best Experience
                        </div>
                        <div className={'w-full font-semibold'}>
                          {datapokemon.pokemon.base_experience}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    </Main >
  );
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { userpokemonId } = context.query;

  return {
    props: {
      userpokemonId: userpokemonId
    }
  };
};


export default Pokemon;