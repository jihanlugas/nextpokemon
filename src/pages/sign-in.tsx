import { useContext } from 'react';
import Head from 'next/head';
import Image from 'next/image';
import AppContext from '@stores/appContext';
import * as Yup from 'yup';
import { Form, Formik, FormikValues } from 'formik';
import { useMutation } from 'react-query';
import TextField from '@com/Formik/TextField';
import ButtonSubmit from '@com/Formik/ButtonSubmit';
import type { NextPage } from 'next';
import { Api } from '@lib/Api';
import Router from 'next/router';

interface Props {

}

let schema = Yup.object().shape({
	username: Yup.string().required(),
	passwd: Yup.string().required(),
});

const Signin: NextPage<Props> = ({ }) => {
	const { notif } = useContext(AppContext);

	const initFormikValue = {
		username: '',
		passwd: '',
	};

	const { data, mutate, isLoading } = useMutation((val: FormikValues) => Api.post('/sign-in', val));

	const handleSubmit = (values: FormikValues, setErrors) => {
		mutate(values, {
			onSuccess: (res) => {
				if (res) {
					if (res.success) {
						Router.push('/');
					} else if (res.error) {
						if (res.payload && res.payload.listError) {
							setErrors(res.payload.listError);
						} else {
							notif.error(res.message);
						}
					}
				}
			},
			onError: (res) => {
				notif.error('Please cek you connection');
			}
		});
	};

	return (
		<div className={'h-screen w-screen bg-gray-200 flex justify-center items-center'}>
			<div className={'px-4 w-full max-w-md'}>
				<div className={'w-full bg-white rounded-lg shadow p-4'}>
					<Formik
						initialValues={initFormikValue}
						validationSchema={schema}
						enableReinitialize={true}
						onSubmit={(values, { setErrors }) => handleSubmit(values, setErrors)}
					>
						{() => {
							return (
								<Form>
									<div className={'flex justify-center'}>
										<span className={'text-xl font-bold'}>Sign In</span>
									</div>
									<div className={''}>
										<div className="mb-4">
											<TextField
												label={'Username'}
												name={'username'}
												type={'text'}
												placeholder={'Username'}
											/>
										</div>
										<div className="mb-4">
											<TextField
												label={'Password'}
												type={'password'}
												name={'passwd'}
												placeholder={'Password'}
											/>
										</div>
										<div className={''}>
											<ButtonSubmit
												label={'Sign In'}
												disabled={isLoading}
												loading={isLoading}
											/>
										</div>
									</div>
								</Form>
							);
						}}
					</Formik>

				</div>
			</div>
		</div>
	);
};

export default Signin;