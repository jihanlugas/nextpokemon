import Main from '@com/Layout/Main';
import type { NextPage } from 'next';

interface Props {

}

const Index: NextPage<Props> = ({ }) => {
	return (
		<Main>
			<div>Index</div>
		</Main>

	);
};

export default Index;