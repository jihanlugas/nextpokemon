import { useContext, useState, useEffect } from 'react'
import { NextPage } from "next";
import AppContext from "@stores/appContext"
import * as Yup from 'yup';
import { Form, Formik, FormikValues, FieldArray } from 'formik';
import { useMutation } from 'react-query'
import Modal from '@com/Modal/Modal';
import TextField from "@com/Formik/TextField";
import TextAreaField from "@com/Formik/TextAreaField";
import ButtonSubmit from "@com/Formik/ButtonSubmit";
import { Api } from '@lib/Api';
import { AiOutlineLoading3Quarters } from 'react-icons/ai'
import { BsTrash } from 'react-icons/bs'


interface Props {
    show: boolean;
    onClickOverlay: Function;
    selectedId: number;
}

interface Init {
    userpokemonId: number,
    nickname: string,
}

let schema = Yup.object().shape({
    nickname: Yup.string().required(),
});

const defaultInit = {
    userpokemonId: 0,
    nickname: "",
}

const ModalMyPokemon: React.FC<Props> = ({ show, onClickOverlay, selectedId }) => {
    const { notif } = useContext(AppContext)
    const [init, setInit] = useState<Init>(defaultInit);

    const FormSave = useMutation((val: FormikValues) => Api.post("/user-pokemon/my-pokemon", val))
    const FormReq = useMutation((id: number) => Api.get("/user-pokemon/my-pokemon/" + id))

    const handleSubmit = (values: FormikValues, setErrors, resetForm) => {

        FormSave.mutate(values, {
            onSuccess: (res) => {
                console.log("save ", res)
                resetForm()
                setInit(defaultInit)
                notif.success(res.message)
                onClickOverlay(0, true)
            }
        })
    }

    useEffect(() => {
        if (selectedId === 0) {
            setInit(defaultInit)
        } else {
            FormReq.mutate(selectedId, {
                onSuccess: (res) => {
                    if (res && res.success) {
                        setInit(res.payload.myPokemon)
                    } else {
                        notif.error(res.message)
                    }
                }
            })
        }
    }, [selectedId])

    return (
        <Modal show={show} onClickOverlay={onClickOverlay}>
            <div className={"p-4"}>
                {FormReq.isLoading ? (
                    <div className={"h-60 flex justify-center items-center"}>
                        <AiOutlineLoading3Quarters className={"animate-spin"} size={"4em"} />
                    </div>
                ) : (
                    <Formik
                        initialValues={init}
                        validationSchema={schema}
                        enableReinitialize={true}
                        onSubmit={(values, { setErrors, resetForm }) => handleSubmit(values, setErrors, resetForm)}
                    >
                        {({ values, resetForm }) => {
                            return (
                                <Form>
                                    <div>
                                        <div className={"flex justify-between items-center mb-4"}>
                                            <span className={"text-xl"}>My Pokemon</span>
                                        </div>
                                        <div className="mb-4">
                                            <TextField
                                                label={"Nickname"}
                                                name={"nickname"}
                                                type={"text"}
                                                placeholder={"nickname"}
                                            />
                                        </div>
                                        <div className={""}>
                                            <ButtonSubmit
                                                label={'Save'}
                                                disabled={FormSave.isLoading}
                                                loading={FormSave.isLoading}
                                            />
                                        </div>
                                    </div>
                                </Form>
                            )
                        }}
                    </Formik>
                )}
            </div>
        </Modal>
    )
}

export default ModalMyPokemon