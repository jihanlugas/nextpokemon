import { useContext, useState, useRef, useEffect } from 'react';
import type { NextPage } from 'next';
import { GoThreeBars } from 'react-icons/go';
import { useMutation } from 'react-query';
import { Api } from '@lib/Api';
import Router from 'next/router';
import AppContext from '@stores/appContext';
import { AiOutlineUser } from 'react-icons/ai';



interface Props {
	onClickOverlay: Function,
}

const Header: NextPage<Props> = ({ onClickOverlay }) => {
	const { notif } = useContext(AppContext);
	const [navbarOpen, setNavbarOpen] = useState(false);
	const [profileBar, setProfileBar] = useState(false);
	const ref = useRef<HTMLInputElement>();

	const { mutate } = useMutation(() => Api.get('/sign-out'));

	const handleLogout = () => {
		mutate(null, {
			onSuccess: (res) => {
				Router.push('/sign-in');
			},

			onError: (res) => {
				notif.error('Please cek you connection');
			}
		});
	};

	useEffect(() => {
		// setProfileBar(!profileBar)
		const checkIfClickedOutside = e => {
			// If the menu is open and the clicked target is not within the menu,
			// then close the menu
			if (profileBar && ref.current && !ref.current.contains(e.target)) {
				setProfileBar(false);
			}
		};

		document.addEventListener('mousedown', checkIfClickedOutside);

		return () => {
			// Cleanup the event listener
			document.removeEventListener('mousedown', checkIfClickedOutside);
		};
	}, [profileBar]);

	return (
		<div className={'mb-16'}>
			<nav className="top-0 fixed w-full flex items-center bg-white shadow h-16 z-10">
				<div className={'flex justify-between w-full items-center'}>
					<div className={'ml-2 flex items-center'}>
						<div className={'flex justify-center items-center w-12 h-12'} onClick={() => onClickOverlay()}>
							<GoThreeBars size={'2em'} />
						</div>
						<span className={'ml-2 text-3xl'}>{process.env.APP_NAME}</span>
					</div>
					<div className="relative inline-block text-left" ref={ref}>
						<div className="flex items-center mr-4">
							<div className="w-10 h-10 bg-gray-300 flex justify-center items-center rounded-full focus:outline-none" onClick={() => setProfileBar(!profileBar)}>
								<AiOutlineUser className={'font-bold'} size={'1.5em'} />
							</div>
						</div>
						<div className={`${profileBar ? 'absolute right-4 mt-2 w-56 rounded-md origin-top-right shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none' : 'hidden'}`}>
							<div className="py-1" role="none">
								<a href="#" className="text-gray-700 block px-4 py-2 text-sm">Account settings</a>
								<a href="#" className="text-gray-700 block px-4 py-2 text-sm">Support</a>
								<a href="#" className="text-gray-700 block px-4 py-2 text-sm">License</a>
								<button className="text-gray-700 block w-full text-left px-4 py-2 text-sm" onClick={() => handleLogout()}>
									Sign out
								</button>
							</div>
						</div>
						{/* <div className={`origin-top-right absolute right-4 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none transition ease-out duration-100 transform opacity-0 scale-95 ${profileBar && ' opacity-100 scale-100'}`}>
                            <div className="py-1" role="none">
                                <a href="#" className="text-gray-700 block px-4 py-2 text-sm">Account settings</a>
                                <a href="#" className="text-gray-700 block px-4 py-2 text-sm">Support</a>
                                <a href="#" className="text-gray-700 block px-4 py-2 text-sm">License</a>
                                <button className="text-gray-700 block w-full text-left px-4 py-2 text-sm" onClick={() => handleLogout()}>
                                    Sign out
                                </button>
                            </div>
                        </div> */}
					</div>
					{/* <div className="mr-4 relative inline-block text-left">
                        <button className="w-10 h-10 bg-red-400 rounded-full focus:outline-none">
                            <div></div>
                        </button>
                        <div className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="menu-button">
                            <div className="py-1" role="none">
                                <a href="#" className="text-gray-700 block px-4 py-2 text-sm">Account settings</a>
                                <a href="#" className="text-gray-700 block px-4 py-2 text-sm">Support</a>
                                <a href="#" className="text-gray-700 block px-4 py-2 text-sm">License</a>
                                <form method="POST" action="#" role="none">
                                    <button type="submit" className="text-gray-700 block w-full text-left px-4 py-2 text-sm">
                                        Sign out
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div> */}
				</div>
			</nav>
		</div>
	);
};

export default Header;