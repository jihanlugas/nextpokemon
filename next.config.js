/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  env: {
    APP_NAME: process.env.APP_NAME,
    API_END_POINT: process.env.API_END_POINT,
  },
  images: {
    domains: [
      "192.168.100.212",
    ]
  }
};
